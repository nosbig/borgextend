#!/usr/bin/env python3

import os
import pwd
import subprocess
import sys


# You can optionally add logging, etc. to log attempts that fail to verify the command enforcement,
# client IPs, etc. via environment variables, etc.

cur_user = os.geteuid()
homedir = pwd.getpwuid(cur_user).pw_dir
os.chdir(homedir)

orig_cmd = sys.argv[1:]
if orig_cmd.pop(0) != 'borg':
    raise PermissionError('That command is not allowed')
if orig_cmd.pop(0) != 'serve':
    raise PermissionError('That command is not allowed')
new_cmd = ['borg', 'serve', '--restrict-to-path', homedir]
new_cmd.extend(orig_cmd)

os.chdir(homedir)
subprocess.run(new_cmd)
